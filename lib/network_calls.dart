import 'package:http/http.dart' as http;

// const String BASE_URL = "http://192.168.0.178:3000";
const String BASE_URL = "https://demo-login-api.herokuapp.com";

Future<String> signUp(
  String firstname,
  String lastname,
  String emailId,
  String phoneNumber,
  String pswd,
  bool isEnableFaceId,
) async {
  if (firstname == null ||
      pswd == null ||
      lastname == null ||
      emailId == null ||
      phoneNumber == null ||
      isEnableFaceId == null) {
    return "Invalid parameters";
  }
  Map data = {
    "firstname": firstname,
    "lastname": lastname,
    "emailId": emailId,
    "phoneNumber": phoneNumber,
    "pswd": pswd,
    "enableFaceId": isEnableFaceId.toString()
  };
  try {
    var response = await http.post(
      BASE_URL + "/signUp",
      body: data,
    );
    return response.body;
  } catch (e) {
    return "Something went wrong.";
  }
}

Future<String> login(
  String username,
  String pswd,
) async {
  if (username == null || pswd == null) {
    return "Invalid parameters";
  }
  Map data = {
    "username": username,
    "pswd": pswd,
  };
  try {
    var response = await http.post(
      BASE_URL + "/login",
      body: data,
    );
    return response.body;
  } catch (e) {
    return "Something went wrong.";
  }
}
