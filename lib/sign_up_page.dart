import 'package:custom_switch_button/custom_switch_button.dart';
import 'package:flutter/material.dart';
import 'package:login_demo/login_page.dart';
import 'package:login_demo/network_calls.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  List<String> countryCodeItems = ['+91', '+92', '+93', '+1', '+088'];
  String selectedCountryCode = "+91";
  bool isEnableFaceId = false, _isSigningUp = false;

  TextEditingController _firstnameController = TextEditingController();
  TextEditingController _lastnameController = TextEditingController();
  TextEditingController _emailIdController = TextEditingController();
  TextEditingController _phoneNoController = TextEditingController();
  TextEditingController _pswdController = TextEditingController();
  TextEditingController _confirmPswdController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    InputBorder border = UnderlineInputBorder(
      borderSide: new BorderSide(color: Colors.white.withOpacity(0.3)),
    );
    return Scaffold(
      backgroundColor: Color(0xff084075),
      body: Container(
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  "Signup",
                  style: TextStyle(fontSize: 20, color: Colors.white),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: _firstnameController,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  decoration: InputDecoration(
                    hintText: "First Name",
                    hintStyle: TextStyle(color: Colors.white, fontSize: 14),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: _lastnameController,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  decoration: InputDecoration(
                    hintText: "Last Name",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: _emailIdController,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    hintText: "Email ID",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    PopupMenuButton(
                      onSelected: (s) {
                        setState(() {
                          selectedCountryCode = s;
                        });
                      },
                      itemBuilder: (BuildContext context) {
                        return countryCodeItems.map((String choice) {
                          return PopupMenuItem(
                            value: choice,
                            child: Text(choice),
                          );
                        }).toList();
                      },
                      child: Column(children: <Widget>[
                        Row(
                          children: [
                            Text(
                              selectedCountryCode,
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              height: 5,
                            ),
                            Icon(Icons.keyboard_arrow_down,
                                color: Colors.white),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          width: 70,
                          color: Colors.white.withOpacity(0.3),
                          height: 1,
                        )
                      ]),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.phone,
                        controller: _phoneNoController,
                        style: TextStyle(color: Colors.white, fontSize: 14),
                        decoration: InputDecoration(
                          hintText: "Phone No.",
                          hintStyle: TextStyle(color: Colors.white),
                          enabledBorder: border,
                          border: border,
                          focusedBorder: border,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: _pswdController,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: "Create Password",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  controller: _confirmPswdController,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: "Confirm Password",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Enable Face ID",
                      style: TextStyle(fontSize: 14, color: Colors.white),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          isEnableFaceId = !isEnableFaceId;
                        });
                      },
                      child: CustomSwitchButton(
                        animationDuration: Duration(milliseconds: 200),
                        backgroundColor: Colors.white,
                        checkedColor: Color(0xff0987F2),
                        checked: isEnableFaceId,
                        unCheckedColor: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 50,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: _isSigningUp
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : ButtonTheme(
                        height: 57,
                        child: RaisedButton(
                          color: Color(0xff0987F2),
                          onPressed: () {
                            if (_firstnameController.text.isEmpty) {
                              showSnackBar(
                                  context,
                                  "Please enter first name to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (_lastnameController.text.isEmpty) {
                              showSnackBar(
                                  context,
                                  "Please enter last name to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (_emailIdController.text.isEmpty) {
                              showSnackBar(
                                  context,
                                  "Please enter email id to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (!(RegExp(
                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                .hasMatch(_emailIdController.text))) {
                              showSnackBar(
                                  context,
                                  "Please enter valid email id to continue",
                                  Colors.red[900]);
                              return;
                            }

                            if (_phoneNoController.text.isEmpty) {
                              showSnackBar(
                                  context,
                                  "Please enter phone number to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (_phoneNoController.text.length != 10) {
                              showSnackBar(
                                  context,
                                  "Please enter valid phone number to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (_pswdController.text.isEmpty) {
                              showSnackBar(
                                  context,
                                  "Please create password to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (_confirmPswdController.text.isEmpty) {
                              showSnackBar(
                                  context,
                                  "Please re-enter created password to continue",
                                  Colors.red[900]);
                              return;
                            }
                            if (_pswdController.text !=
                                _confirmPswdController.text) {
                              showSnackBar(
                                  context,
                                  "Entered passwords not matched",
                                  Colors.red[900]);
                              return;
                            }
                            setState(() {
                              _isSigningUp = true;
                            });
                            signUp(
                                    _firstnameController.text,
                                    _lastnameController.text,
                                    _emailIdController.text,
                                    selectedCountryCode +
                                        _phoneNoController.text,
                                    _pswdController.text,
                                    isEnableFaceId)
                                .then((value) {
                              setState(() {
                                _isSigningUp = false;
                              });
                              if (value == "OK") {
                                showSnackBar(context, "Sign Up successfull",
                                    Colors.green);
                              } else {
                                showSnackBar(context, value, Colors.red[900]);
                              }
                            });
                          },
                          child: Text(
                            "SIGNUP",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
