import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:login_demo/network_calls.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _usernameController = TextEditingController();
  TextEditingController _pswdController = TextEditingController();
  String _usernameErrorText, _pswdErrorText;
  bool _isLoggingIn = false;
  @override
  Widget build(BuildContext context) {
    InputBorder border = UnderlineInputBorder(
      borderSide: new BorderSide(color: Colors.white.withOpacity(0.3)),
    );
    return Scaffold(
      backgroundColor: Color(0xff084075),
      body: Container(
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: [
              Padding(
                padding: EdgeInsets.all(100),
                child: Image.asset(
                  "images/login_logo.png",
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  controller: _usernameController,
                  decoration: InputDecoration(
                    errorText: _usernameErrorText,
                    hintText: "Username",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  style: TextStyle(color: Colors.white, fontSize: 14),
                  controller: _pswdController,
                  obscureText: true,
                  decoration: InputDecoration(
                    errorText: _pswdErrorText,
                    hintText: "Password",
                    hintStyle: TextStyle(color: Colors.white),
                    enabledBorder: border,
                    border: border,
                    focusedBorder: border,
                  ),
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: _isLoggingIn
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : ButtonTheme(
                        height: 57,
                        child: RaisedButton(
                          color: Color(0xff0987F2),
                          onPressed: () {
                            setState(() {
                              _usernameErrorText = null;
                              _pswdErrorText = null;
                            });
                            if (_usernameController.text.isEmpty) {
                              setState(() {
                                _usernameErrorText = "Please enter username";
                              });
                              return;
                            }
                            if (_pswdController.text.isEmpty) {
                              setState(() {
                                _pswdErrorText = "Please enter password";
                              });
                              return;
                            }
                            setState(() {
                              _isLoggingIn = true;
                            });
                            login(_usernameController.text,
                                    _pswdController.text)
                                .then((value) {
                              setState(() {
                                _isLoggingIn = false;
                              });
                              if (value == "OK") {
                                showSnackBar(context, "Logged In Successful",
                                    Colors.green);
                              } else {
                                showSnackBar(context, value, Colors.red[900]);
                              }
                            });
                          },
                          child: Text(
                            "LOGIN",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: FlatButton.icon(
                  onPressed: () {},
                  icon: Image.asset("images/login_with_face_id.png"),
                  label: Text(
                    "Login with Face ID",
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: InkWell(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(
                            text: "Not having an account?",
                            style: TextStyle(
                              color: Colors.white.withOpacity(0.3),
                            ),
                          ),
                          TextSpan(
                            text: " Create one",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, "/signup");
                  },
                ),
              ),
              SizedBox(
                height: 100,
              )
            ],
          ),
        ),
      ),
    );
  }
}

showSnackBar(BuildContext context, String text, Color backgroundColor) {
  Flushbar(
    message: text,
    duration: Duration(seconds: 2),
    backgroundColor: backgroundColor,
  )..show(context);
}
